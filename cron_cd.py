# from ChatGPT
# needs work:
# - state info from last run from/to file
# - class-based implementation
# - docstrings, type hints, formatting etc
# - unit tests
# - parameterize which checks/tests are run
# - CLI interface

import os
import subprocess
from github import Github

# Set up your personal access token
access_token = os.environ['ACCESS_TOKEN']

# Set up the repository details
repo_owner = 'owner_name'
repo_name = 'repository_name'

# Set up the preconfigured branches
preconfigured_branches = ['branch1', 'branch2']

# Create a Github instance using the personal access token
g = Github(access_token)

def run_test():
    result = subprocess.run(['python', 'test.py'], capture_output=True)
    status = result.returncode
    error_msg = result.stderr.decode().strip()
    if not error_msg:
        error_msg = None
    return status, error_msg

def trigger_workflow(pull, status, error_msg):
    data = {'status': status, 'error': error_msg}
    pull.create_issue_comment(f'Test result: {status}\nError message: {error_msg}')
    repo.get_workflow('test.yml').create_dispatch(inputs=data)

# Get the repository
repo = g.get_repo(f'{repo_owner}/{repo_name}')

# Check for a new pull request
pulls = repo.get_pulls(state='open', sort='created', base='master')
if pulls.totalCount > 0:
    pull = pulls[0]
    print(f'New pull request: {pull.title} ({pull.html_url})')
    status, error_msg = run_test()
    trigger_workflow(pull, status, error_msg)
else:
    print('No new pull request found')

# Check for a push to a branch for which an open pull request exists
for pull in repo.get_pulls(state='open'):
    head_ref = pull.head.ref
    head_sha = pull.head.sha
    for event in repo.get_events():
        if event.event == 'push' and event.payload['ref'] == f'refs/heads/{head_ref}' and event.payload['after'] == head_sha:
            print(f'New push to branch {head_ref}')
            status, error_msg = run_test()
            trigger_workflow(pull, status, error_msg)
        else:
            print('No push to a branch for which an open pull request exists')

# Check for a push to a preconfigured branch
for event in repo.get_events():
    if event.event == 'push' and event.payload['ref'] in preconfigured_branches:
        print(f'New push to preconfigured branch {event.payload["ref"]}')
        status, error_msg = run_test()
        trigger_workflow(None, status, error_msg)
    else:
        print('No push to a preconfigured branch')
